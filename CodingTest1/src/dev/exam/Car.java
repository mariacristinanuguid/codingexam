package dev.exam;

public class Car extends Vehicle {

	@Override
	public void getType() {
		System.out.println("4-Wheeled Car");
	}
	
	public void getType(String color) {
		System.out.println(color);
	}
	
	public static void main(String[] args) {
		Car car = new Car();
		car.getType();
		car.getType("Red");
		
		Vehicle vehicle = new Car();
		vehicle.getType();
		
		Vehicle vehicle2 = new Vehicle();
		vehicle2.getType();
		
	}

	
}
