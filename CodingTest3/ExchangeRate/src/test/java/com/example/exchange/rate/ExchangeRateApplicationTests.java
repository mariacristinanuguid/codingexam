package com.example.exchange.rate;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.exchange.rate.entity.Currency;
import com.example.exchange.rate.entity.ExchangeRate;
import com.example.exchange.rate.service.CurrencyService;
import com.example.exchange.rate.service.ExchangeRateService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ObjectNode;

@SpringBootTest
@AutoConfigureMockMvc
class ExchangeRateApplicationTests {

	@Autowired
    private ExchangeRateService exchangeRateService;
	
	@Autowired
	private CurrencyService currencyService;
	
	
	@Test
	public void getCurrenctExchangeRate() {
		List<Currency> currencies = currencyService.findAll();
		if(currencies != null) {
			currencies.forEach(currency -> {
				ObjectNode node = null;
				try {
					node = exchangeRateService.getExchangeRate(currency.getCurrency());
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(node.has("conversion_rates")) {
					System.err.println(node.get("conversion_rates").toString());
					ExchangeRate rate = new ExchangeRate();
					rate.setBaseCurrency(currency);
					rate.setConversionRate(node.get("conversion_rates").toString());
					System.err.println(rate);
					exchangeRateService.create(rate);
				}
			});
		}
	}

}
