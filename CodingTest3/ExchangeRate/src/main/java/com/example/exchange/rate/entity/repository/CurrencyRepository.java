package com.example.exchange.rate.entity.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.exchange.rate.entity.Currency;

public interface CurrencyRepository extends JpaRepository<Currency, Long>{

	Optional<Currency> findByCurrency(String currency);
}
