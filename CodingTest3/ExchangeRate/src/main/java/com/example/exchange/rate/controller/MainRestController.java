package com.example.exchange.rate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.exchange.rate.entity.ExchangeRate;
import com.example.exchange.rate.service.ExchangeRateService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
@RequestMapping(path="/ExhangeRate")
public class MainRestController {

	@Autowired
	private ExchangeRateService exchangeRateService;
	
	@GetMapping(value="",produces="application/json")
	public JsonNode getExchange(@RequestParam String base,@RequestParam(required = false) String target) throws Exception {
		if(target == null) {
			return exchangeRateService.getRealTime(base);			
		}else {
			return exchangeRateService.getPairConversion(base, target);
		}
	}
	
//
//	@GetMapping(value="/PairConversion",produces="application/json")
//	public ObjectNode getExchange(@RequestParam String base,) throws JsonMappingException, JsonProcessingException {
//		return exchangeRateService.getRealTime(base);
//	}
}
