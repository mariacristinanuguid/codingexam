package com.example.exchange.rate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.example.exchange.rate.entity.Currency;
import com.example.exchange.rate.service.CurrencyService;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {

	@Autowired
	private CurrencyService currencyService;
	
	@Override
	public void run(String... args) throws Exception {
		
		Currency currency = currencyService.findByCurrency("USD");
		if(currency == null) {
			currency = new Currency();
			currency.setCurrency("USD");
			currencyService.create(currency);
		}
		
		currency = currencyService.findByCurrency("KRW");
		if(currency == null) {
			currency = new Currency();
			currency.setCurrency("KRW");
			currencyService.create(currency);
		}
		
		currency = currencyService.findByCurrency("JPY");
		if(currency == null) {
			currency = new Currency();
			currency.setCurrency("JPY");
			currencyService.create(currency);
		}
	}

}
