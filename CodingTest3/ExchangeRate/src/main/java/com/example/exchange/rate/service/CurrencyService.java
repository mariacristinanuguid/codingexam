package com.example.exchange.rate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.exchange.rate.entity.Currency;
import com.example.exchange.rate.entity.repository.CurrencyRepository;

@Service
public class CurrencyService {

	@Autowired
	private CurrencyRepository currencyRepository;
	
	public Currency create(Currency c) {
		return currencyRepository.save(c);
	}
	
	public Currency findByCurrency(String currency) {
		Optional<Currency> optional = currencyRepository.findByCurrency(currency);
		return optional.isPresent() ? optional.get() : null;
	}
	
	public List<Currency> findAll(){
		return currencyRepository.findAll();
	}
	
	
}
