package com.example.exchange.rate.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.exchange.rate.entity.ExchangeRate;

public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, Long>{

}
