package com.example.exchange.rate.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class ExchangeRate {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	
	@OneToOne(fetch=FetchType.LAZY)
	private Currency baseCurrency;
	
	@Column(nullable = true)
	private String convertedTo;

	@Column(columnDefinition = "text")
	private String conversionRate;
	
	

	public String getConvertedTo() {
		return convertedTo;
	}

	public void setConvertedTo(String convertedTo) {
		this.convertedTo = convertedTo;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Currency getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(Currency baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public String getConversionRate() {
		return conversionRate;
	}

	public void setConversionRate(String conversionRate) {
		this.conversionRate = conversionRate;
	}

	@Override
	public String toString() {
		return "ExchangeRate [id=" + id + ", baseCurrency=" + baseCurrency + ", conversionRate=" + conversionRate + "]";
	}
	
	
	
	
	
}
