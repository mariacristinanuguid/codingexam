package com.example.exchange.rate.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.exchange.rate.entity.Currency;
import com.example.exchange.rate.entity.ExchangeRate;
import com.example.exchange.rate.entity.repository.ExchangeRateRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Service
public class ExchangeRateService {
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private ExchangeRateRepository exchangeRateRepository;
	@Autowired
	private CurrencyService currencyService;

	public ObjectNode getExchangeRate(String currency) throws JsonMappingException, JsonProcessingException {
		RestTemplate restTemplate = new RestTemplate();
		String apiUrl = "https://v6.exchangerate-api.com/v6/af31f1e257d1fcaecf640dd7/latest/"+currency;
		System.err.println(apiUrl);
		ResponseEntity<String> response = restTemplate.getForEntity(apiUrl , String.class);
		System.err.println(response.getBody());
		return (ObjectNode) mapper.readTree(response.getBody());
		
		
	}
	
	
	public ExchangeRate create(ExchangeRate rate) {
		return exchangeRateRepository.save(rate);
	}
	
	public ObjectNode getRealTime(String base) throws JsonMappingException, JsonProcessingException {
		Currency currency = currencyService.findByCurrency(base.toUpperCase());
		if(currency == null) {
			currency = new Currency();
			currency.setCurrency(base);
			currency = currencyService.create(currency);
		}
		
		
		RestTemplate restTemplate = new RestTemplate();
		String apiUrl = "https://v6.exchangerate-api.com/v6/af31f1e257d1fcaecf640dd7/latest/"+currency.getCurrency();
		System.err.println(apiUrl);
		ResponseEntity<String> response = restTemplate.getForEntity(apiUrl , String.class);
		JsonNode node =  mapper.readTree(response.getBody());
		
		ExchangeRate rate = new ExchangeRate();
		rate.setBaseCurrency(currency);
		if(node.has("conversion_rates")) {
			rate.setConversionRate(node.get("conversion_rates").toString());
		}
		
		ObjectNode tmp = mapper.createObjectNode();
		tmp.put("base_code",currency.getCurrency());
		tmp.set("conversion_rates", node.get("conversion_rates"));
		 create(rate);
		
		 return tmp;
	}
	
	public JsonNode getPairConversion(String base,String convertTo) throws Exception {
		Currency currency = currencyService.findByCurrency(base.toUpperCase());
		if(currency == null) {
			currency = new Currency();
			currency.setCurrency(base);
			currency = currencyService.create(currency);
		}
		
		RestTemplate restTemplate = new RestTemplate();
		String apiUrl = "https://v6.exchangerate-api.com/v6/af31f1e257d1fcaecf640dd7/pair/"+currency.getCurrency()+"/"+convertTo.toUpperCase();
		ResponseEntity<String> response = restTemplate.getForEntity(apiUrl , String.class);
		System.err.println(response.getBody());
		JsonNode node =  mapper.readTree(response.getBody());
		
		ExchangeRate rate = new ExchangeRate();
		rate.setBaseCurrency(currency);
		if(node.has("conversion_rate")) {
			rate.setConversionRate(node.get("conversion_rate").toString());
		}
		rate.setConvertedTo(convertTo.toUpperCase());
		
		ObjectNode tmp = mapper.createObjectNode();
		tmp.put("base_code",currency.getCurrency());
		tmp.set("conversion_rates", node.get("conversion_rate"));
		tmp.put("target_code", convertTo.toUpperCase());
		 create(rate);
		
		 return tmp;
	}
}
