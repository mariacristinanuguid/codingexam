# CodingExam

a. For Coding Exam 3

	1. To get current exchange rate GET `localhost:3006/ExhangeRate?base=`+ currency
	2. To get current exchange rate conversion GET `localhost:3006/ExhangeRate?base=`+ currency +`&target=`+targetcurrency
	
b. For Coding Exam 4

	1. To Create User : POST
		`localhost:3007/User`
			-Request Body : `{
								firstName:'',
								lastName:';
							}`
	2. To get User details : GET
		`localhost:3007/User/`+user id
	3. To get all users : GET 
		`localhost:3007/User/All`
	4. To deposit to specific user : POST 
		`localhost:3007/Transaction/Deposit`
			Request Body : `{
								    "user_id": 4,
									"amount":1000,
							}`
	5. To withdraw to specific user : POST
		`localhost:3007/Transaction/Withdraw`
				Request Body : `{
										"user_id": 4,
										"amount":1000,
								}`
	6.  To transfer to specific user : POST
		`localhost:3007/Transaction/Transfer`
				Request Body : `{
										"user_id": 4,
										"amount":50,
										"trasnfer_to_user_id":7
								}`
	7. To view transaction history of user : GET
		-`localhost:3007/TransactionHistory?user_id=`+user_id
	8. To view specific type of transaction history of user : GET
		-`localhost:3007/TransactionHistory?user_id=`+user_id+`&type=`+TYPE (DEPOSIT,WITHDRAW,TRANSFER)
