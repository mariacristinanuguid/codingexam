package dev.exam;

public class Consumer extends Thread {

	 Stock stock;
     int size;

     public Consumer(Stock b, int size) {
         this.stock = b;
         this.size = size;
     }

     public void run() {
    	 int i = 0;
         while (i != 10) {

             while (stock.empty()) {
                 synchronized (stock) {
                     try {
                         System.out.println("Stock empty");
                         stock.wait();
                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                 }
             }

             String dataitem = null;
             synchronized (stock) {
                 dataitem = stock.delete();
             }
             if(dataitem == null) {
            	 System.out.println("Stock empty");
             }else {
            	 System.out.println("Removed " + dataitem);
            	 
             }
                 i++;
         }
     }

}
