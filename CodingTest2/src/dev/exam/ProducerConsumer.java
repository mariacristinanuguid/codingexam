package dev.exam;

public class ProducerConsumer {

	public static void main(String[] args) {
        new ProducerConsumer();
    }

    public ProducerConsumer() {
        int size = 5;

        Stock stock = new Stock(size);

        Thread prod = new Thread(new Producer(stock, size));
        Thread cons = new Thread(new Consumer(stock, size));

        prod.start();
        cons.start();
    }
}
