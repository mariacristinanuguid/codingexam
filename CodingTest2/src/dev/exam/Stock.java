package dev.exam;

public class Stock {

	private String products[];
	private int currentPosition;
	
	public Stock( int maximumQuantity) {
		this.products = new String[maximumQuantity];
		this.currentPosition = -1;
	}
	
	public synchronized boolean insert(String item) {
		int i = (currentPosition + 1) % products.length;
		
		if(currentPosition == products.length -1) {
			System.out.println("Products full");
			return false;
		}else{
			currentPosition = i;
			products[currentPosition] = item;
			return true;
		}
	}
	
	public boolean empty() {
		return currentPosition == -1;
	}
	
	public synchronized String delete() {
      String result = products[currentPosition];
      if(currentPosition != -1) {
    	  currentPosition = (currentPosition + 1) % products.length;
      }
      return result;
	}

}
