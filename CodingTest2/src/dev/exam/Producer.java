package dev.exam;

public class Producer extends Thread {

    Stock stock;
    int size;

    public Producer(Stock b, int size) {
        this.stock = b;
        this.size = size;
    }

    public void run() {
        int i = 0;
        while (i != 10) {
            try {
                String dataitem =   "Stock NO." + ++i;
                boolean bool = stock.insert(dataitem);
                if (bool) {
                    System.out.println("Successfully inserted " + dataitem);
                }
                synchronized (stock) {
                    stock.notifyAll();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}