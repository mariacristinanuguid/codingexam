package com.example.accounting.entity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.accounting.entity.APIResponse;
import com.example.accounting.entity.User;
import com.example.accounting.entity.service.UserService;
import com.fasterxml.jackson.databind.JsonNode;

@RestController
@RequestMapping(path = "/User")
public class UserRestController{

	@Autowired
	private UserService userService;
	
	@PostMapping(value="",produces = "application/json")
	public APIResponse create(@RequestBody User u) throws Exception {
		return userService.create(u);
	}
	
	@GetMapping(value="/All",produces = "application/json")
	public APIResponse get() {
		return userService.findAll();
	}
	

	@GetMapping(value="/{id}",produces = "application/json")
	public APIResponse getSpecificUser(@PathVariable long id) {
		return userService.retrieveUser(id);
	}
}
