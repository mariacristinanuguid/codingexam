package com.example.accounting.entity.service;

import com.example.accounting.entity.APIResponse;
import com.fasterxml.jackson.databind.JsonNode;

public class MyService {

	public APIResponse generateResponse(JsonNode node) {
		APIResponse response = new APIResponse();
		response.setStatus("OK");
		response.setData(node);
		
		return response;
	}
}
