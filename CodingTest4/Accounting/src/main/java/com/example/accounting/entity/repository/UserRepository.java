package com.example.accounting.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.accounting.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
