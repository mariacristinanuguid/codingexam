package com.example.accounting.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "ignoreUnknown", "true"})
public class APIResponse {

	private String status;
	private JsonNode data;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public JsonNode getData() {
		return data;
	}
	public void setData(JsonNode data) {
		this.data = data;
	}
	
	
	
	
}
