package com.example.accounting.entity.enums;

public enum TransactionStatus {

	PENDING,APPROVED,REJECTED,CANCELLED
}
