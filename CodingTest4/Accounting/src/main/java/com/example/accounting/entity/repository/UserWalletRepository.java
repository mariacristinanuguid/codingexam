package com.example.accounting.entity.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.accounting.entity.User;
import com.example.accounting.entity.UserWallet;

public interface UserWalletRepository extends JpaRepository<UserWallet, Long> {

	List<UserWallet> findAllByUser(User user);
	Optional<UserWallet> findByNameAndUser(String name,User  user);
	
	
	
}
