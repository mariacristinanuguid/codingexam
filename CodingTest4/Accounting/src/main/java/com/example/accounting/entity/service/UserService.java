package com.example.accounting.entity.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.example.accounting.entity.APIResponse;
import com.example.accounting.entity.User;
import com.example.accounting.entity.UserWallet;
import com.example.accounting.entity.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Service
public class UserService extends MyService implements CrudService<User> {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private UserWalletService userWalletService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public APIResponse create(User u) throws Exception {
		u = userRepository.save(u);
		
		UserWallet wallet = new UserWallet();
		wallet.setBalance(0);
		wallet.setName("Main wallet");
		wallet.setUser(u);
		
		wallet = userWalletService.createUserWallet(wallet);
		ObjectNode node = (ObjectNode) u.toJson(mapper);
		node.set("userWallet", wallet.toJson(mapper));
		return generateResponse(node);
	}
	
	public APIResponse findAll(){
		ArrayNode node = mapper.createArrayNode();
		
		List<ObjectNode> data = jdbcTemplate.query("select u.*,uw.balance from user u JOIN user_wallet  uw on uw.user_id = u.id", new RowMapper<ObjectNode> () {

			@Override
			public ObjectNode mapRow(ResultSet rs, int rowNum) throws SQLException {
				ObjectNode node = mapper.createObjectNode();
				node.put("id", rs.getLong("id"));
				node.put("firstName", rs.getString("first_name"));
				node.put("lastName", rs.getString("last_name"));
				node.put("balance", rs.getDouble("balance"));
				return node;
			}
			
		});
		
		if(data != null) {
			node.addAll(data);
		}
		
		return generateResponse(node);
	}
	
	public User retrieve(long id) {
		return userRepository.findById(id).orElse(null);
	}

	@Override
	public User update(User t) throws Exception {
		
		return null;
	}
	
	public APIResponse retrieveUser(long id) {
		User user = retrieve(id);
		if(user == null) {
			throw new NullPointerException("User not found.");
		}
		return generateResponse(user.toJson(mapper));
	}
}
