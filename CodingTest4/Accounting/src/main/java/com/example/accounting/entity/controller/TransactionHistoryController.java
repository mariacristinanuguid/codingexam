package com.example.accounting.entity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.accounting.entity.APIResponse;
import com.example.accounting.entity.service.TransactionHistoryService;
import com.fasterxml.jackson.databind.JsonNode;

@RestController
@RequestMapping(path = "/TransactionHistory")
public class TransactionHistoryController{

	@Autowired
	private TransactionHistoryService transactionHistoryService;
	
	@GetMapping(value ="",produces="application/json")
	public APIResponse get(@RequestParam long userId,@RequestParam(required = false) String type) {
		if(type == null) {
			return transactionHistoryService.getTransactionHistoryByUser(userId);
		}else {
			return transactionHistoryService.getTransactionHistoryByUser(userId, type);
		}
	}
}
