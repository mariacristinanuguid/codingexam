package com.example.accounting.entity.service;

import java.util.InputMismatchException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.accounting.entity.APIResponse;
import com.example.accounting.entity.TransactionHistory;
import com.example.accounting.entity.User;
import com.example.accounting.entity.enums.TransactionStatus;
import com.example.accounting.entity.enums.TransactionType;
import com.example.accounting.entity.repository.TransactionHistoryRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

@Service
public class TransactionHistoryService extends MyService implements CrudService<TransactionHistory>{

	@Autowired
	private TransactionHistoryRepository transactionHistoryRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Override
	public APIResponse create(TransactionHistory t) throws Exception {
		// TODO Auto-generated method stub
		return generateResponse(transactionHistoryRepository.save(t).toJson(mapper));
	}

	@Override
	public TransactionHistory retrieve(long id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TransactionHistory update(TransactionHistory t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public APIResponse getTransactionHistoryByUser(long userId) {
		User user = userService.retrieve(userId);
		if(user == null) {
			throw new NullPointerException("User not found.");
		}
		List<TransactionHistory> transactions = transactionHistoryRepository.findAllByUser(user);
		
		ArrayNode node = mapper.createArrayNode();
		if(transactions != null) {
			transactions.forEach(transaction -> {
				node.add(transaction.toJson(mapper));
			});
		}
		
		return generateResponse(node);
	}
	
	public APIResponse getTransactionHistoryByUser(long userId,String type) {
		User user = userService.retrieve(userId);
		if(user == null) {
			throw new NullPointerException("User not found.");
		}
		
		TransactionType transType = null;
		try {
			transType = TransactionType.valueOf(type.toUpperCase());
		}catch(Exception e) {
			throw new InputMismatchException("Type not found.");
		}
		
		List<TransactionHistory> transactions = transactionHistoryRepository.findAllByUserAndType(user,transType);
		
		ArrayNode node = mapper.createArrayNode();
		if(transactions != null) {
			transactions.forEach(transaction -> {
				node.add(transaction.toJson(mapper));
			});
		}
		
		return generateResponse(node);
	}
	
	public APIResponse create(User user,TransactionType type,double amount) throws Exception {
		TransactionHistory history = new TransactionHistory();
		history.setAmount(amount);
		history.setTransactionStatus(TransactionStatus.PENDING);
		history.setType(type);
		history.setUser(user);
		
		return create(history);
	}

	
}
