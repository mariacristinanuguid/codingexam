package com.example.accounting.entity.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.accounting.entity.APIResponse;
import com.example.accounting.entity.User;
import com.example.accounting.entity.UserWallet;
import com.example.accounting.entity.enums.TransactionType;
import com.example.accounting.entity.repository.UserWalletRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

@Service
public class UserWalletService extends MyService implements CrudService<UserWallet> {

	
	@Autowired
	private UserWalletRepository userWalletRepository;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TransactionHistoryService transactionHistoryService;
	
	@Override
	public APIResponse create(UserWallet t) throws Exception {
		// TODO Auto-generated method stub
		return generateResponse(userWalletRepository.save(t).toJson(mapper));
	}
	
	public UserWallet createUserWallet(UserWallet t) throws Exception {
		// TODO Auto-generated method stub
		return userWalletRepository.save(t);
	}

	@Override
	public UserWallet retrieve(long id) throws Exception {
		// TODO Auto-generated method stub
		return userWalletRepository.findById(id).orElse(null);
	}

	@Override
	public UserWallet update(UserWallet t) throws Exception {
		// TODO Auto-generated method stub
		return userWalletRepository.save(t);
	}
	
	@Transactional(rollbackOn = Exception.class)
	public APIResponse deposit(JsonNode params) throws Exception {
		if(!params.has("user_id")) {
			throw new NullPointerException("User ID is required.");
		}
		User user = userService.retrieve(params.get("user_id").asLong());
		if(user == null) {
			throw new NullPointerException("User not found.");
		}
		
		Optional<UserWallet> optional = userWalletRepository.findByNameAndUser("Main wallet", user);
		UserWallet wallet = null;
		if(!optional.isPresent()) {
			throw new NullPointerException("Wallet not found.");
		}else {
			wallet = optional.get();
		}
		
		if(!params.has("amount")) {
			throw new NullPointerException("Amount is required.");
		}
		
		double amount = params.get("amount").asDouble();
		
		wallet.setBalance(wallet.getBalance()+amount);
		
		wallet = update(wallet);
		
		transactionHistoryService.create(user, TransactionType.DEPOSIT, amount);
		
		return generateResponse(wallet.toJson(mapper));
	}
	

	@Transactional(rollbackOn = Exception.class)
	public APIResponse withdraw(JsonNode params) throws Exception {
		if(!params.has("user_id")) {
			throw new NullPointerException("User ID is required.");
		}
		User user = userService.retrieve(params.get("user_id").asLong());
		if(user == null) {
			throw new NullPointerException("User not found.");
		}
		
		
		
		Optional<UserWallet> optional = userWalletRepository.findByNameAndUser("Main wallet", user);
		UserWallet wallet = null;
		if(!optional.isPresent()) {
			throw new NullPointerException("Wallet not found.");
		}else {
			wallet = optional.get();
		}
		
		if(!params.has("amount")) {
			throw new NullPointerException("Amount is required.");
		}
		
		double amount = params.get("amount").asDouble();
		
		double balance = wallet.getBalance() - amount;
		if(balance < 0) {
			throw new Exception("Withdraw amount is greather than wallet balance.");
		}
		
		wallet.setBalance(balance);
		
		wallet = update(wallet);
		
		transactionHistoryService.create(user, TransactionType.WITHDRAW, amount);
		
		return generateResponse(wallet.toJson(mapper));
	}

	public APIResponse getUserWallet(long userId) {
		User user = userService.retrieve(userId);
		if(user == null) {
			throw new NullPointerException("User not found.");
		}
		
		ArrayNode node = mapper.createArrayNode();
		List<UserWallet> wallets = userWalletRepository.findAllByUser(user);
		if(wallets != null) {
			wallets.forEach(wallet -> {
				node.add(wallet.toJson(mapper));
			});
		}
		return generateResponse(node);
	}
	
	public APIResponse transfer(JsonNode params) throws Exception {
		if(!params.has("user_id")) {
			throw new NullPointerException("User ID is required.");
		}
		User user = userService.retrieve(params.get("user_id").asLong());
		if(user == null) {
			throw new NullPointerException("User not found.");
		}
		
		if(!params.has("trasnfer_to_user_id")) {
			throw new NullPointerException("Transfer to user ID is required.");
		}
		
		User transferTouser = userService.retrieve(params.get("trasnfer_to_user_id").asLong());
		if(transferTouser == null) {
			throw new NullPointerException("User not found.");
		}
		
		
		Optional<UserWallet> userWalletOptional = userWalletRepository.findByNameAndUser("Main wallet", user);
		UserWallet wallet = userWalletOptional.get();
		
		
		Optional<UserWallet> transferTouserWalletOptional = userWalletRepository.findByNameAndUser("Main wallet", transferTouser);
		UserWallet transferTouserWallet = transferTouserWalletOptional.get();
		
		if(!params.has("amount")) {
			throw new NullPointerException("Amount is required.");
		}
		
		double amount = params.get("amount").asDouble();
		
		if(amount > wallet.getBalance()) {
			throw new Exception("Transfer amount is greater than user balance.");
		}
		
		transferTouserWallet.setBalance(transferTouserWallet.getBalance() + amount);
		transferTouserWallet = update(transferTouserWallet);
		
		wallet.setBalance(wallet.getBalance() - amount);
		wallet = update(wallet);
		transactionHistoryService.create(user, TransactionType.TRANSFER, amount);
		
		return generateResponse(wallet.toJson(mapper));
		
		
	}
}
