package com.example.accounting.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.accounting.entity.TransactionHistory;
import com.example.accounting.entity.User;
import com.example.accounting.entity.enums.TransactionType;

public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, Long> {

	List<TransactionHistory> findAllByUser(User user);
	
	List<TransactionHistory> findAllByUserAndType(User user,TransactionType type);
}
