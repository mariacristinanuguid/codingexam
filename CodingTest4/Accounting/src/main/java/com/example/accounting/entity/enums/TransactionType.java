package com.example.accounting.entity.enums;

public enum TransactionType {

	DEPOSIT,WITHDRAW,TRANSFER
}
