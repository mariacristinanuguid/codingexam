package com.example.accounting.entity.service;

import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.example.accounting.entity.APIResponse;

@EnableTransactionManagement
public interface CrudService<T> {

	public APIResponse create(T t) throws Exception;
	public T retrieve(long id) throws Exception;
	public T update(T t) throws Exception;
}
