package com.example.accounting.entity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.accounting.entity.APIResponse;
import com.example.accounting.entity.service.UserWalletService;
import com.fasterxml.jackson.databind.JsonNode;

@RestController
@RequestMapping(path = "/Transaction")
public class Transaction {
	
	@Autowired
	private UserWalletService userWalletService;
	
	@PostMapping(value="/Deposit",produces = "application/json")
	public APIResponse deposit(@RequestBody JsonNode params) throws Exception {
		return userWalletService.deposit(params);
	}
	
	
	@PostMapping(value="/Withdraw",produces = "application/json")
	public APIResponse withdraw(@RequestBody JsonNode params) throws Exception {
		return userWalletService.withdraw(params);
	}
	

	
	@PostMapping(value="/Transfer",produces = "application/json")
	public APIResponse transfer(@RequestBody JsonNode params) throws Exception {
		return userWalletService.transfer(params);
	}

}
