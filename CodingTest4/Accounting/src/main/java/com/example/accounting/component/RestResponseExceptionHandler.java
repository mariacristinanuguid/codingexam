package com.example.accounting.component;

import java.net.SocketException;


import org.apache.http.conn.HttpHostConnectException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {
  
   
   @ExceptionHandler(Exception.class)
   protected ResponseEntity<Object> handleEntityNotFound(Exception ex) {
       ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR);
       apiError.setMessage(ex.getMessage());
       
       apiError.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    
       return buildResponseEntity(apiError);
   }
   
   
   private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
       return new ResponseEntity<>(apiError, HttpStatus.OK);
   }

}